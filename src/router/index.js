import Vue from 'vue'
import VueRouter from 'vue-router'
import loading from '@/views/loading.vue'
import home from '@/views/home.vue'
import preview from '@/views/preview.vue'
import choose from '@/views/choose.vue'
import paint from '@/views/paint.vue'
import poster from '@/views/poster.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'loading',
        component: loading
    },
    {
        path: '/home',
        name: 'home',
        component: home
    },
    {
        path: '/preview',
        name: 'preview',
        component: preview
    },
    {
        path: '/choose',
        name: 'choose',
        component: choose
    },
    {
        path: '/paint',
        name: 'paint',
        component: paint
    },
    {
        path: '/poster',
        name: 'poster',
        component: poster
    }
    // {
    //     path: '/about',
    //     name: 'About',
    //     // route level code-splitting
    //     // this generates a separate chunk (about.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
]

const router = new VueRouter({
    routes
})

export default router
