import Vue from 'vue'
import App from './App.vue'
import router from './router'

// vendor
import ydui from '../public/vendor/ydui.flexible.js'; // 页面分辨率

import Vconsole from 'vconsole'; // vconsole
const vConsole = new Vconsole();
export default vConsole;

Vue.config.productionTip = false

new Vue({
  	router,
  	render: h => h(App),
  	data() {
  		return {
  			paintURL:'',
  			postURL:''
  		}
  	}
}).$mount('#app')
