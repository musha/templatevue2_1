// console.log('page poster');

import html2canvas from 'html2canvas';
export default {
	data() {
		return {
			count: 0,
			isshow: false,
			loop: '',
			second: 0
		}
	},
	mounted() {
		// var context = this;
		let imgs = [
	        require("@/assets/images/logo.png"),
	        require("@/assets/images/topbrand.png"),
	        require("@/assets/images/qrcode1.png"),
	        require("@/assets/images/otherbg.png"),
	        require("@/assets/images/bg.png"),
	        require("@/assets/images/inter.png"),
	        this.$root.paintURL
	    ];
	    for(let img of imgs) {
	    	let image = new Image();
	    	image.src = img;
	    	image.onload = () => {
	    		this.count++;
	    		if(this.count == 7) {
	    			html2canvas(this.$refs.postbg, {
	    				useCORS: true,
	    				allowTaint: true,
	    				taintTest: false
	    			}).then(canvas => {
	    				this.$root.postURL = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
	    				// 获取生成的图片的url
		                let img = document.createElement("img");
		                img.src = this.$root.paintURL
		                img.onload = () => {
		                    this.isshow = true
		                }
	    			});
	    		};
	    	};
	    };
	},
	methods: {
		//
        close(type) {
        	//
        },
        touchstart() {
        	//
        },
        touchend() {
        	//
        }
	}
};
