// console.log('page paint');

import swiper from '../../../public/vendor/swiper/swiper.min.js';
import '../../../public/vendor/swiper/swiper.min.css';
import html2canvas from 'html2canvas';
export default {
	data() {
		return {
			panSwiper: '',
			paintIndex: '',
			color: '',
			domes: []
		};
	},
	mounted() {
		console.log(this.$route.query.choose);
		this.paintIndex = this.$route.query.choose;
		let context = this;
		context.$nextTick(() => {
			context.panSwiper =  new Swiper('.box-swiper', {
				loop: true,
				prevButton: '.swiper-button-prev',
				nextButton: '.swiper-button-next',
				onSlideChangeEnd: function(swiper) {
					// console.log(swiper.realIndex);
				},
				onClick: function(swiper, e) {
					// console.log(e.target.id);
					context.choosePan(e.target.id);
				}
			});
		});
	},
	methods: {
		//
		goBack() {
			this.$router.replace('/choose');
		},
		choosePan(color) {
			// console.log(color)
			let aaa = '',
				bbb = '';
			if(this.color != '') {
				document.getElementsByClassName(this.color).forEach((de) => {
					de.style.marginTop = '9%';
				});
			};
			aaa = document.getElementsByClassName(color);
			aaa.forEach((demo) => {
				demo.style.marginTop = '0%';
			});
			this.color = color;
		},
		changeColor(name) {
			if(this.color == '') {
				// this.$dialog.toast({
				// 	mes: '请先选择一个画笔噢',
				// 	timeout: 1500
				// });
				alert('请先选择一个画笔噢');
			} else {
				this.domes = document.getElementsByClassName(name);
				// console.log(this.domes);
				this.domes.forEach((demo) => {
					demo.style.fill = this.color;
				});
			};
		},
		reset() {
			// var context = this;
			this.paintIndex = -1;
			setTimeout(() => {
				this.paintIndex = this.$route.query.choose;
			}, 100);
		},
		createPost() {
			let ww = window.getComputedStyle(this.$refs.paintbox).width;
			let hh = window.getComputedStyle(this.$refs.paintbox).height;
			this.$refs.paintbox.setAttribute('width', ww);
        	this.$refs.paintbox.setAttribute('height', hh);
        	this.$nextTick(() => {
        		if(this.color == '') {
        			alert('还没有涂色');
        		} else {
        			// console.log(this.$refs.paintbox);
        			html2canvas(this.$refs.paintbox, {
        				useCORS: true
        			}).then((canvas) => {
        				// console.log(canvas);
					    this.$root.paintURL = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
					    // 获取生成的图片的url
                    	console.log(this.$root.paintURL);
                    	this.$router.replace({
                            path:'/poster'
                        });
					}).catch(e => {
						//
					});
        		};
        	});
		}
	}
};
