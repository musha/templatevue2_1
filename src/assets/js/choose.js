// console.log('page choose');

import swiper from '../../../public/vendor/swiper/swiper.min.js';
import '../../../public/vendor/swiper/swiper.min.css';
export default {
	data() {
		return {
			index: 0
		};
	},
	mounted() {
		var context = this;
		var swiper = new Swiper('.box-swiper .maps', {
			loop: true,
			prevButton: '.swiper-button-prev',
			nextButton: '.swiper-button-next',
			onSlideChangeEnd: function(swiper) {
				// console.log(swiper.realIndex);
				context.index = swiper.realIndex;
				// console.log(this.index);
			}
		});
	},
	methods: {
		//
		goBack() {
			this.$router.replace('/preview');
		},
        toPaint() {
            this.$router.replace({
            	path: `/paint?choose=${this.index}`
            });
        }
	}
};
